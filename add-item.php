<?php 
session_start();
if(!isset($_SESSION['user'])){
    header('Location: login.php');
}
$titel="add item";
    include('header.php'); 
?>
<?php
if(isset($_POST['add']))
{
    $ID=$_POST['pid'];
    $Name= $_POST['name'];
    $Price=$_POST['price'];
    $filename = $_FILES['image']['name'];
    $tmp = $_FILES['image']['tmp_name'];
    $error = $_FILES['image']['error'];
    $type = $_FILES['image']['type'];
    $size = $_FILES['image']['size'];
    
    $ext = end(explode(".",$filename));
    $path = "img/stuff/stuff-$ID.$ext";
    
    $accepted = array("png","jpg","jpeg");
    
    $filetype = explode("/",$type);
    
    if(!$error and $size<=512000 and $filetype[0]=="image" and in_array($ext, $accepted))
    {
        $out = move_uploaded_file($tmp,$path);
        
        if($out){
            include('coneect.php');
            $query1 = "INSERT into stuff set id=$ID, name='$Name', img='$path', price=$Price";
            $result = mysqli_query($con, $query1);
        }
        else {
            $result = 0;
        }
    }
    else {
        $result = 0;
    }
    header('Location: add-item.php?status='.$result);
    exit();
}
?>
    

<div id="content">
<div class="page section">
    <h1>Add Item</h1>
        <?php if(isset($_GET['status']) and $_GET['status']==1) { ?>
        <p>Item added to the datebase</p>
        <?php } else if(isset($_GET['status']) and $_GET['status']==0) { ?>
        <p>Item not added to the datebase</p>
        <?php } else { ?>
    
                <form id="contact" name="add" method="POST" action="add-item.php" enctype="multipart/form-data">
                    <table>
                        <tr>
                            <th>
                                <label for="pid">Product-ID*</label>
                            </th>
                            <td>
                                <input type="text" name="pid" id="pid">
                                
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <label for="name">Product Name*</label>
                            </th>
                            <td>
                                <input type="text" name="name" id="name">
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <label for="image">Upload Image</label>
                            </th>
                            <td>
                                <input type="file" name="image" id="image">
                                <span style="font-size: 0.6em; font-style: italic;">(Maximum 500 KB)</span>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <label for="price">Price*</label>
                            </th>
                            <td>
                                <input type="text" name="price" id="price">
                                
                            </td>
                        </tr>                    
                    </table>
                    <input type="submit" value="Add" name="add" id="add"/>

                </form>
    <?php } ?>
    </div>
</div>
<?php
    include('footer.php');
?>
