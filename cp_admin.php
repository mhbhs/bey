<?php 
session_start();
if(!isset($_SESSION['user'])){
    header('Location: login.php');
}
$title='Admin Control Panel'; ?>
<?php $section='cp_admin'; ?>
<?php include('header.php'); ?>

		<div class="section shirts latest">

			<div class="wrapper">

				<h2>Administrator Control Panel</h2>
                       <div style="width: 50%; margin: 0 auto;">
				<ul class="products">
					<li><a href="add-item.php">
							<img src="add.jpg">
							<p>Add Items</p>
						</a>
					</li><li>
						<a href="modify-item.php">
							<img src="update.jpg">
							<p>Modify Items</p>
						</a>
					</li>								
				</ul>
                       </div>
			</div>

		</div>

	</div>
  <?php include('footer.php'); ?>     
